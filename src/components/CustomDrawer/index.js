import * as React from 'react'
import {
    View,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    Text,
    StyleSheet,
    Animated,
    ImageBackground,
    Keyboard
} from 'react-native'
import {
    Drawer
} from 'react-native-paper';

export default function CustonDrawer({navigation}) {

    async function atividades(){
        navigation.navigate('Atividades');
    }
    async function calendarios(){
        navigation.navigate('Calendarios');
    }
    async function cardapios(){
        navigation.navigate('Cardapios');
    }
    async function financeiro(){
        navigation.navigate('Financeiro');
    }
    async function recados(){
        navigation.navigate('Recados');
    }
    async function rotinas(){
        navigation.navigate('Rotinas');
    }
    return (
        <View style={styles.container}>
            <View style={styles.containerLogo}>
                <Image 
                    style={styles.imagemLogo}
                    source={require('../../../assets/logo.png')}
                />
            </View>
            <View style={styles.itens}>
                <Drawer.Item
                    style={styles.dintem}
                    icon='note-outline'
                    label="Atividades"
                    onPress={atividades}
                />
                <Drawer.Item
                    style={styles.dintem}
                    icon='calendar-month'
                    label="Calendarios"
                    onPress={calendarios}
                />
                <Drawer.Item
                    style={styles.dintem}
                    icon='silverware-fork-knife'
                    label="Cardapios"
                    onPress={cardapios}
                />
                <Drawer.Item
                    style={styles.dintem}
                    icon='cash'
                    label="Financeiro"
                    onPress={financeiro}
                />
                <Drawer.Item
                    style={styles.dintem}
                    icon='square-edit-outline'
                    label="Recados"
                    onPress={recados}
                />
                <Drawer.Item
                    style={styles.dintem}
                    icon='history'
                    label="Rotinas"
                    onPress={rotinas}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    containerLogo:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imagemLogo:{
        width: 130,
        height: 82,
    },
    container:{
        marginTop: '40%'
    },
    itens:{
        borderTopWidth: 1,
        borderTopColor: '#f1f1f1',
        marginTop: '24%', 
        paddingTop: 15
    },
    dintem:{
        padding: 10,
        
    }
});