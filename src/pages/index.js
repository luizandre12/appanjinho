import React, {useState, useEffect, } from 'react';
import { 
  View, 
  KeyboardAvoidingView, 
  Image, 
  TextInput, 
  TouchableOpacity, 
  Text, 
  StyleSheet,
  Animated,
  ImageBackground,
  Keyboard,
  BackHandler
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'
import AsyncStorage from '@react-native-community/async-storage';


export default function Login({navigation}) {  

    async function recados(){
        navigation.navigate('Recados');
    }
    async function cardapios(){
        navigation.navigate('Cardapios');
    }
    async function atividades(){
        navigation.navigate('Atividades');
    }
    async function rotinas(){
        navigation.navigate('Rotinas');
    }
    async function calendarios(){
        navigation.navigate('Calendarios');
    }
    async function financeiro(){
        navigation.navigate('Financeiro');
    }

    async function sair(){
        await AsyncStorage.clear();
        navigation.navigate('Login');
    }

    const [foto, setFoto] = useState('');
    const [nome, setNome] = useState('');
    const [turma, setTurma] = useState('');

    useEffect(()=> {
        BackHandler.addEventListener('hardwareBackPress', () => true);
        AsyncStorage.getItem('foto').then(foto => {
            if(foto){
                setFoto(foto);
            }else{
                setFoto('padrao.jpg');
            }
        })
        AsyncStorage.getItem('nameAluno').then(nameAluno => {
            setNome(nameAluno);
        })
        AsyncStorage.getItem('turma').then(turma => {
            setTurma(turma);
        })
        
    }, []);

    return (
        <ImageBackground 
        source={require('../../assets/fundo.png')} 
        style={styles.fundo}
        >
        <View style={styles.background}>
            <View style={styles.header}>
                <View>
                    <TouchableOpacity onPress={sair}>
                        <Icon name="sign-out-alt" style={styles.sair} size={25} color="#634225" />
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity>
                    <Icon name="sign-out-alt" style={styles.menu} size={1} color="#ebf4f1" />
                    </TouchableOpacity>
                </View>
            </View>
                
            <View style={styles.containerLogo}>
                <Image 
                    style={styles.imagemLogo}
                    source={require('../../assets/logo.png')}
                />
            </View>

            <View style={styles.containerAluno}>
                <Image 
                    style={styles.imagemAluno}
                    source={{
                        uri: 'http://anjinhodaguarda.com.br/sistema/img/alunos/' + foto,
                    }}
                />
                <Text style={styles.nomeAluno}>{nome}</Text>
                <Text style={styles.nomeTurma}>{turma}</Text>
            </View>

            <View style={styles.paginaMenu}>
                <View style={styles.rowMenu}>
                    <TouchableOpacity style={styles.btnMenuL} onPress={recados}>
                        <Icon name="edit" size={30} color="#1eb4a8" />
                        <Text style={styles.btnTexto}>
                            Recados
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.btnMenuR} onPress={cardapios}>
                        <Icon name="utensils" size={30} color="#1eb4a8" />
                        <Text style={styles.btnTexto}>
                            Cardápio
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.rowMenu}>
                    <TouchableOpacity style={styles.btnMenuL} onPress={atividades}>
                        <Icon name="chalkboard-teacher" size={30} color="#1eb4a8" />
                        <Text style={styles.btnTexto}>
                            Atividades
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.btnMenuR} onPress={rotinas}>
                        <Icon name="history" size={30} color="#1eb4a8" />
                        <Text style={styles.btnTexto}>
                            Rotinas
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.rowMenu}>
                    <TouchableOpacity style={styles.btnMenuL} onPress={calendarios}>
                        <Icon name="calendar-alt" size={30} color="#1eb4a8" />
                        <Text style={styles.btnTexto}>
                            Calendário
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnMenuR} onPress={financeiro}>
                        <Icon name="dollar-sign" size={30} color="#1eb4a8" />
                        <Text style={styles.btnTexto}>
                            Financeiro
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    // style padrao
    fundo:{
        flex: 1,
        resizeMode: "cover",
        width: '100%'
    },
    header:{
        backgroundColor: '#ebf4f1',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: '12%'
    },
    background:{
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
    },
    containerLogo:{
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        position: 'absolute',
        marginTop: '13%'
    },
    imagemLogo:{
        width: 130,
        height: 82,
    },

    // style da pagina
    sair:{
        marginLeft: '25%',
        marginTop: '45%'
    },
    menu:{
        marginRight: '7%',
        marginTop: '115%'
    },
    containerAluno:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '10%'
    },
    imagemAluno:{
        width: 180,
        height: 180,
        borderRadius: 100
    },
    nomeAluno:{
        color: '#634225',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 10,
        marginRight: 15,
        marginLeft: 15
    },
    nomeTurma:{
        color: '#FFF',
        fontSize: 20,
        // fontWeight: 'bold',
        textAlign: 'center',
        // marginTop: 10,
        marginRight: 15,
        marginLeft: 15
    },
    rowMenu:{
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        
    },
    btnMenuL:{
        backgroundColor: '#FFF',
        width: '43%',
        height: 80,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        marginLeft: 30,
        borderRadius: 10
    },
    btnMenuR:{
        backgroundColor: '#FFF',
        width: '43%',
        height: 80,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        marginRight: 15,
        borderRadius: 10
    },
    btnTexto:{
        color: '#634225',
        fontWeight: "bold",
        fontSize: 20
    },
    paginaMenu:{
        flex: 1
    }
});