import React, {useState, useEffect, Component} from 'react';
import { 
  View, 
  KeyboardAvoidingView, 
  Image, 
  TextInput, 
  TouchableOpacity, 
  Text, 
  StyleSheet,
  Animated,
  ImageBackground,
  Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


import axios from 'axios';

export default function Login({navigation}) {

  const [email, setEmail] = useState('');
  const [senha, setSenha] = useState('');
  const [offset] = useState(new Animated.ValueXY({x: 0, y: 400}));
  const [opacity] = useState(new Animated.Value(0));
  const [logo] = useState(new Animated.Value(1));
  const [logoOffset] = useState(new Animated.ValueXY({x: 0, y: -325}));
  const [footerOffset] = useState(new Animated.ValueXY({x: 0, y: 0}));
  const [tamanho, setTamanho] = useState(0);
  const [marginInput, setMarginInput] = useState(105);
  const [logoW, setLogoW] = useState(320);
  const [logoH, setLogoH] = useState(200);
  const [logoM, setLogoM] = useState(0);
  var fotoAluno = '';


  async function loadUsers() {
    const res = await axios.get("http://anjinhodaguarda.com.br/sistema/api/users.php?login=" + email.trim() + "&senha=" + senha)
    if(res.data == 'erro'){
      setTamanho(1);
    }else{
      if(res.data.usuario[0].foto != null){
        fotoAluno = res.data.usuario[0].foto;
      }
      // console.log(res.data)
      await AsyncStorage.setItem('nameAluno', res.data.usuario[0].name);
      await AsyncStorage.setItem('classroom_id', res.data.usuario[0].classroom_id);
      await AsyncStorage.setItem('foto', fotoAluno);
      await AsyncStorage.setItem('idAluno', res.data.usuario[0].id);
      await AsyncStorage.setItem('institution_id', res.data.usuario[0].institution_id);
      await AsyncStorage.setItem('idMatricula', res.data.usuario[0].matricula);
      await AsyncStorage.setItem('escola', res.data.usuario[0].nomeEscola);
      await AsyncStorage.setItem('turma', res.data.usuario[0].nomeTurma);
      await AsyncStorage.setItem('login', email);
      await AsyncStorage.setItem('password', senha);
      navigation.navigate('Index');
    }


  }

  async function handleSubmit(){
    loadUsers();
  }

  useEffect(()=> {

    AsyncStorage.getItem('login').then(login => {
      if(login){
        navigation.navigate('Index');
      }
    })

    KeyboardDidShowListener = Keyboard.addListener('keyboardDidShow', keyboardDidShow)
    KeyboardDidHideListener = Keyboard.addListener('keyboardDidHide', keyboardDidHide)

    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 1,
        bounciness: 0,
        useNativeDriver: true,
      }),
      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }),
      Animated.spring(logoOffset.y, {
        toValue: 0,
        speed: 1,
        bounciness: 0,
        useNativeDriver: true,
      })
    ]).start();

  }, []);

  function keyboardDidShow(){
    Animated.spring(footerOffset.y, {
      toValue: 100,
      speed: 4,
      useNativeDriver: true,
    }).start();
    setLogoW(200);
    setLogoH(126);
    setTamanho(0);
    setMarginInput(0);
    setLogoM(50)
  }
  
  function keyboardDidHide(){
    Animated.spring(footerOffset.y, {
      toValue: 0,
      speed: 4,
      bounciness: 0,
      useNativeDriver: true,
    }).start();
    setLogoW(320);
    setLogoH(200);
    setMarginInput(105);
    setLogoM(0)
  }

  return (
    <ImageBackground 
      source={require('../../assets/fundo.png')} 
      style={styles.fundo}
    >
      <KeyboardAvoidingView style={styles.background}>
        <View style={styles.containerLogo}>
          <Animated.Image 
            style={
              {
                width: logoW,
                height: logoH,
                marginBottom: logoM,
                opacity: logo,
                transform:[
                  { translateY: logoOffset.y }
                ]
              }
            }
            source={require('../../assets/logo.png')}
          />
        </View>

        <Animated.View 
          style={[
            styles.container,
            {
              opacity: opacity,
              marginBottom: marginInput,
              transform:[
                { translateY: offset.y }
              ]
            }  
          ]}
        >
          <TextInput 
            style={[styles.input, {borderWidth: tamanho}]}
            placeholder="Usuário"
            placeholderTextColor="#634225"
            value={email}
            onChangeText={setEmail}
            autoCapitalize="none"
            
          />
          
          <TextInput 
            style={[styles.input, {borderWidth: tamanho}]}
            placeholder="Senha"
            placeholderTextColor="#634225"
            autoCorrect={false}
            value={senha}
            onChangeText={setSenha}
            secureTextEntry={true}
          />

          <TouchableOpacity style={styles.btnEntrar} onPress={handleSubmit}>
            <Text style={styles.btnTexto}>Entrar</Text>
          </TouchableOpacity>
        </Animated.View>

        <Animated.View 
          style={[
            styles.footer,
            {
              transform:[
                { translateY: footerOffset.y }
              ]
            }
          ]}>
          <Text style={styles.textFooter}>(65) 3642-7103 - (65) 99917-7103</Text>
        </Animated.View>
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  background:{
    flex: 1,
    alignItems: 'center',
    alignContent: 'center',
    // backgroundColor: '#95ced8'
  },
  containerLogo:{
    flex: 1,
    justifyContent: 'center',
    marginTop: 70
  },
  container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '90%',
    
  },
  input:{
    backgroundColor: '#fff',
    width: '80%',
    marginBottom: 15,
    color: '#634225',
    fontSize: 17,
    borderRadius: 10,
    paddingLeft: 15,
    height: 40,
    fontWeight: "bold",
    borderColor: '#FF0000',
  },
  btnEntrar:{
    backgroundColor: '#634225',
    width: '25%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15
  },
  btnTexto:{
    color: '#FFF',
    fontWeight: "bold"
  },
  footer:{
    backgroundColor: '#ebf4f1',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 25,
    paddingBottom: 25,
  },
  textFooter:{
    color: '#634225',
    fontSize: 15,
    fontWeight: "bold"
  },
  fundo:{
    flex: 1,
    resizeMode: "cover",
    width: '100%'
  }

});