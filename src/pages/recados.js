import React, {useState, useEffect} from 'react';
import { 
  View, 
  KeyboardAvoidingView, 
  Image, 
  TextInput, 
  TouchableOpacity, 
  Text, 
  StyleSheet,
  Animated,
  ImageBackground,
  Keyboard
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'

export default function Recados({navigation}) {  

    async function voltar(){
        navigation.navigate('Index');
    }
    async function menuD(){
        navigation.openDrawer()
    }

    return (
        <ImageBackground 
            source={require('../../assets/fundo.png')} 
            style={styles.fundo}
        >
        <View style={styles.background}>
            <View style={styles.header}>
                <View>
                    <TouchableOpacity onPress={voltar}>
                        <Icon name="arrow-left" style={styles.voltar} size={30} color="#634225" />
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={menuD}>
                        <Icon name="bars" style={styles.menu} size={30} color="#634225" />
                    </TouchableOpacity>
                </View>
            </View>
                
            <View style={styles.containerLogo}>
                <Image 
                    style={styles.imagemLogo}
                    source={require('../../assets/logo.png')}
                />
            </View>
        </View>
        
        <View style={styles.containerFundinho}>
            <Image 
                style={styles.imagemFundinho}
                source={require('../../assets/fundinho.png')}
            />
        </View>
        <View style={styles.titulo}>
            <Text style={styles.tituloPagina}>Recados</Text>
        </View>

        <View style={styles.containerConteudo}>
            <View style={styles.blocoConteudo}>
                <Text style={styles.dataConteudo}>01/01/2021</Text>
                <Text style={styles.textoConteudo}>Olá o Rafael esta semana está fazendo muita bagunça, se tiver como falar com ele pois está atrapalhando muito os outros coleguinhas</Text>
            </View>
        </View>



        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    // style padrao
    fundo:{
        flex: 1,
        resizeMode: "cover",
        width: '100%'
    },
    header:{
        backgroundColor: '#ebf4f1',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: '12%'
    },
    background:{
        flex: 1,
        alignItems: 'center',
    },
    containerLogo:{
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        position: 'absolute',
        marginTop: '13%'
    },
    imagemLogo:{
        width: 130,
        height: 82,
    },
    voltar:{
        marginLeft: '25%',
        marginTop: '44%'
    },
    menu:{
        marginRight: '7%',
        marginTop: '105%'
    },
    containerFundinho:{
        position: 'absolute',
        marginTop: '38%'
    },
    imagemFundinho:{
        height: 40,
    },
    titulo:{
        position: 'absolute',
        marginTop: '39%',
        marginLeft: '7%'
    },
    tituloPagina:{
        fontSize: 25,
        color: '#634225',
        fontWeight: 'bold'
    },

    // style da pagina
    containerConteudo:{
        position: 'absolute',
        marginTop: 220,
        alignItems: 'center'
    },
    blocoConteudo:{
        backgroundColor: '#fff',
        width: 370,
        marginLeft: '5%',
        borderRadius: 10,
        padding: 15
    },
    dataConteudo:{
        color: '#634225',
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 15
    },
    textoConteudo:{
        color: '#297e8d',
        fontSize: 17,
        fontWeight: 'bold'
    }
});