import {
    createAppContainer,
    createSwitchNavigator
} from 'react-navigation';
import {
    createDrawerNavigator
} from 'react-navigation-drawer'

import Login from './pages/login';
import Rotinas from './pages/rotinas';
import Recados from './pages/recados';
import Index from './pages/index';
import Financeiro from './pages/financeiro';
import Cardapios from './pages/cardapios';
import Calendarios from './pages/calendarios';
import Atividades from './pages/atividades';
import CustonDrawer from './components/CustomDrawer'

const Routes = createAppContainer(
    createDrawerNavigator({
        Login,
        Index,
        Atividades,
        Calendarios,    
        Cardapios,
        Financeiro,
        Recados,
        Rotinas
    }, {
        initialRouteName: 'Login',
        drawerPosition: 'right', 
        contentComponent: CustonDrawer
    }),
);

export default Routes;