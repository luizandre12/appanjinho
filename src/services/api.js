import axios from 'axios';

const api = axios.create({
    baseURL: 'http://anjinhodaguarda.com.br/sistema/api',
});

export default api;